# Contributing

I have high demands and expectactions of the quality. All contributed code is welcomed, however, I personally will read all changed data.

If you want to contribute, simply fork and edit the code, and open an [merge request](https://gitlab.com/tukusejssirs/martyrology/-/merge_requests/new). If you just want to report an issue or ask a question, simply open an [issue](https://gitlab.com/tukusejssirs/martyrology/-/issues/new).

Note that I test the CSS stylesheet in Firefox on Linux and on Android only. Also note that I use [Libertinus Serif](https://github.com/alerque/libertinus) font as the main font.

## Quick guide to proof and style the text

Below I provide you quick guide how I proof and style the text and how anyone could do it. Note that this guide is only valid for the main part of the text (pp77-694).

You can start proofing and localising any page you want, but note that I personally am proofing the text from the beginning to the end without skipping any page, therefore it might be a good idea to tell me (via an issue) that you are about to start proofing and styling _a specific range of pages_ (like a month or page range) in order I could skip them. This might be also helpful for other potential contributors.

I require proofreading the text as thoroughly as possible to the original printed book (MR2004), even with typos and errors, however, if you notice a typo or an error, please either raise an issue or create a MR updating [`errata.md`](https://gitlab.com/tukusejssirs/martyrology/-/blob/master/errata.md). After proofing and styling the whole book, I’d like to create a version of MR2004 with all those typos and errors fixed.

You are encouraged to update [`scripts/regex_replaces_typos.sh`](https://gitlab.com/tukusejssirs/martyrology/-/blob/master/scripts/regex_replaces_typos.sh) file, but it is not obligatory.

### Steps to proofread and style a page

The the following guide I presume you want to proofread and style p117. It will also assume your current working directory is the repository root folder.

1. Open `st_out/117_mr2004.tif` in an image viewer. You’ll correct MR2004 text according to the text in this image.

2. Open `hocr/117_mr2004.html` in a web browser. This step is not mandatory, but it is recommended to view the modified file output.

3. In terminal (in Linux, WSL; you need to have installed BASH and GNU `sed`) run `scripts/regex_replaces_typos.sh hocr/117_mr2004.html`. This will fix some more common typos.

4. Open `hocr/117_mr2004.html` in a text editor. One for editing code would help you fixing the errors, like [Sublime Text](https://www.sublimetext.com/) (which I use) or [VS Code](https://code.visualstudio.com/).

5. Read the file, compare the content with the text in the image, fix the text. Be as thoroughful to the text as possible. While fixing the errors you should the document with the following CSS classes:
   - paragraph classes (add them to `class` of `<p>` tag):
      - header (where the page number is) should be hidden using: `hide_header`;
      - month heading (when a new month starts): `heading_2`;
      - Gregorian date (e.g. `Die 26 ianuarii`): `heading_3_day`;
      - lunar date (e.g. `Séptimo Kaléndas februárii. Luna:`): `lunar_day_name`;
      - footnotes: `footnote`;
      - when a paragraph starts on one page and ends on the other:
         - first part of the paragraph: `prepend_to_next_paragraph` + comment out all whitespaces (newlines and tabs) after the last word of that paragraph until `</body>` tag;
         - second part of the paragraph: `append_to_previous_paragraph` + comment out all whitespaces (newlines and tabs) from `<body>` tag to `<span>` of the first word of that paragraph;
   - word classes (add them to `class` of `<span>` tag of the particular word):
      - red text (e.g. in header): `red_foreground_colour`;
      - italics: `italics`;
      - footnote references (both in text and before footnotes): `footnote_reference`;
      - ordinal numbers (of eulogies): `bold`;
      - the first word of the eulogy (after the ordinal number): `reduce_indent_eulogies`;

6. At the beginning of every day, there is lunar table (it contains letters and numbers). I have create another BASH script that generates it (more or less), but it is not fully automated. You need to copy the original code (from `<div>` containing `ocr_carea` class to its closing `</div` tag); it must contain _exactly_:
   - one `<div>`;
   - one `<p>`;
   - four `<span>` tags with `ocr_line` class, which contain _at least_ one `<span>` with `ocrx_word` class.

If there are less or more `<span>` tags, you need to add/remove them. Then:
   a. copy the whole `<div>`;
   b. look at the lunar table in the image and note down the first number (under lowercase `a`; e.g. `27`)
   c. run `scripts/format_lunar_cycle.sh 'copied text here' 27` (replace `copied text here` with the content you copied in a.; also replace `27` with the number noted in b.);
   d. copy the whole output;
   e. replace the original (copied) output with new one (generated by the script);
   f. indent the code with tabs (a code text editor surely has a command for that, no need to do it manually).

7. When you are finished (i.e. fixed all the errors), add `<!-- done -->` on the very first line of `hocr/117_mr2004.html` (`<!DOCTYPE html>` should be on the second line). This is used to count how many and which pages are proofread and styled.

8. Open a merge request.

## 1&emsp;Proofreading rules

:information_source: These rules are not an exhausted list. The list might be changed without prior notice.

1. OCR’d text in [`hocr/mr2004.html`](/hocr/mr2004.html) and the text only version (created as CI artefact) should one-to-one copy of the printed text, incl potential typos and mistakes.

2. In the LaTeX version, the text should be the same as in original version (HTML/hOCR or text version), however, the known typos and mistakes should be corrected and the text should be formatted using LaTeX (and therefore the header should be removed from each page). Also the text in red, italics, bold, small caps should be styled accordingly. Note that the LaTeX version is not yet available.

3. Ligatures (e.g. `oe`, `ae`) in [`hocr/mr2004.html`](/hocr/mr2004.html) should be kept as in the printed text (e.g. in [`st_out/004_mr2004.tif`](/st_out/004_mr2004.tif), `Apostolicae` is written without ligature, however, `Librariæ` with liguture).

4. Each word should have the correct language property (`lang`; e.g. in [`st_out/004_mr2004.tif`](/st_out/004_mr2004.tif), `Libreria Editrice Vaticana, Città del Vaticano` is in Italian; some place/personal names might also be in another language).

5. Use HTML entities for the following characters:
   - thin space (`U+2009`): `&thinsp;` (it is used after opening angle quote (`«`) and before closing angle quote (`»`);
   - soft hyphen (`U+00AD`): `&shy;` (it is used when a word is hyphenated at the end of a line).

6. Modify hOCR rectangle to cover all characters of a word (etc). Note that the rectangle does not need to be 100 % exact, only approximately. Note that this rule is not yet fully in force and therefore there are many places which either use the same rectangle place and size as another `<span>` or have totally different place/size.

7. Small caps in hOCR and text files should be styled as lowercase letters. Naturally, what is uppercase is small caps should be writter in uppercase.

8. I don’t really care if two words are in one rectangle box separated by a space, or in two rectangle boxes. The latter is nice to have though.

9. The values in tables are grouped (boxed) by cells, even those that are multiline (like `Annus\nDomini` in table header on p26; the newline is replaced with a space). In a cell, there might be more than one box, but the contents of two or more cells cannot be grouped together in a single box.

10. Each word on a line has to be in the same textline (`baseline`). This also true of table rows. Note that this rule is not yet fully in force and therefore there are many places which either use the same rectangle place and size as another `<span>` or have totally different place/size.

11. Currently, I don’t care about about dimensions of text block / paragraph and textline boxes.

12. Drop letters should have a separate rectangle box and thus a separate `span`.

13. Unwanted space between `span`s should be removed by commenting out the indentation. No space character should be used only `<!--` and the end of a line and `-->` after the indentation on the following line. This is used, e.g., in a hyphenated word after `&shy;`.

14. Files (`hocr/*.html`) that are proofed should have `<!-- done -->` on the first line. This is used to help generate a progress report.

15. Thin spaces are always to be non-breakable, therefore we should always use `<span style="white-space: nowrap">&thinsp;</span>`.

## 2&emsp;CSS style rules

:information_source: These rules are not an exhausted list. The list might be changed without prior notice.

1. Alphabetically order CSS properties.

2. Footnote references in hOCR and text files (both those in the text and in the footnotes) should be in a rectangle box in hOCR as the preceding word with the `footnote_reference` CSS class.

3. Although the text is more important than form in this project, the following CSS classes must be used as character styles (manual input is required; these are usually applied to `span`s):
   - `small_caps` for small capitals;
   - `red_foreground_colour` for text in red foreground colour (although not for drop letters or title which are already in red);
   - `normal_foreground_colour` for text in normal (usually black) foreground colour;
   - `normal_background_colour` for text in normal (usually white) background colour;
   - `italics` for italic text;
   - `bold` for bold text;
   - `drop_letters` for drop letters.

4. As I’d like to keep the text clean from any formatting, the headings (titles) should be also set via CSS classes to the `p` tags. Use the following classes as paragraph styles:
   - `half_title` for the title on the half-title page;
   - `title` for the title on the title page;
   - `subtitle` for the subtitle on the title page; note that the last line should have `subtitle_last_line` class assigned;
   - `title_editio` for the edition on the title page;
   - `title_publisher_and_date` for the publisher and date (year) on the title page;
title_verso_editio_on_newline
   - `title_publisher_and_date` for the second line of edition on the title page verso;
   - `title_verso_copyright_owner` for the copyright owner on the title page verso;
   - `heading_1` for heading 1 (part titles on whole page);
   - `heading_2` for heading 2 (for titles always on the top of the page, without header and greater top margin);
   - `heading_2_number` for the number of heading 2; note that this is styled in Roman numerals and is a separate line/paragraph;
   - `heading_3` for heading 3;
   - `heading_3_number` for the number of heading 3; note that this is styled in Roman numerals and is a separate line/paragraph;
   - `heading_3_alt` eulogy chants headings and day headings in the main part of the book;
   - `heading_4` for heading 4;
   - `rubrics` for rubrics (red text); note that on the second paragraph (and the following) use _also_ `rubrics_second_paragraph`;
   - `prayer_large_bold_paragraph` for the text of prayers (larger text in bold); note that all main `span`s (i.e. those that contain `span`s with text) need to have `prayer_large_bold_span` class assigned;
   - `prayer_normal_paragraph` for the text of prayers (normal text); note that all main `span`s (i.e. those that contain `span`s with text) need to have `prayer_normal_span` class assigned;
   - `stanza_style_paragraph` for the text of prayers (larger text); note that all main `span`s (i.e. those that contain `span`s with text) need to have `stanza_style_span` class assigned;
   - `personal_title_center` (used below decrees);
   - `personal_title_center_at_75_percent` (used below decrees);
   - `hide_header` (used for book page headers to hide them; use it for `p` HTML tag);
   - `heading_4_notes` (used for notes in _Elogia pro celebrationibus mobilibus_);
   - `heading_4_pre_note` (used in _Lectiones breves_ as a note before heading 4)
   - `footnotes` for footnotes;
   - `footnote_reference` for footnote references (the numbers);
   - `alternative_reading` for alternative readings header (`Vel:` and biblical reference) (used in _Lectiones breves_).

5. Additional paragraph style overrides (use as required):
   - `greater_space_above_paragraph`;
   - `center_text`;
   - `center_text_at_75_percent`;
   - `prepend_to_next_paragraph` and `append_to_previous_paragraph` (they remove any space after and before the paragraph respectively; note that a newline is still kept as I have not found a pure CSS way to remove it);
   - `reduce_indent_one_digit` and `reduce_indent_two_digits` are used in _Orationes_ (pp63-68) in order to move the prayer numbers a bit to the left and thus align the first line of text with the rest of the prayers.

## 3&emsp;Other rules

:information_source: These rules are not an exhausted list. The list might be changed without prior notice.

1. Use [`.editorconfig`](https://editorconfig.org/).

2. Pages `071_mr2004.tif` through `074_mr2004.tif` are merged into one `071_mr2004.html` because I did not want the JavaScript functions (that is used to create and update the Gregorian chant) to be in one than one file. Also _Elogium In Nativitate Domini. Textus cum cantu_ spreads all four pages and it would be quite difficult to separate the chant by pages.

3. As for the Gregorian chant generation, I chose [`exsurge`](https://github.com/bbloomf/exsurge). Note that this is a bit enhanced downstream fork of the [original one](https://github.com/frmatthew/exsurge) made by [@frmatthew](https://github.com/frmatthew). That said, I have found some bugs when I wanted to change the foreground and background colour to green and black respectively, therefore the chant the the green on black version (default) is not as perfect I’d wish. Any help is greatly appreciated.