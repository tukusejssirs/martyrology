# Processing the book

## Scanning

The book was scanned at 600 dpi resolution using Canon CanoScan LiDE 220.

I used the following command to scan the data:

```bash
scanimage -d <device> -p --resolution 600 --mode Color png --format=png > <filename>.png
```

However, I have create a BASH script in order to ease the batch scanning.

Note that the scanned images are not shared publicly, because each page is about 50 MiB large.

## ScanTailor

Actually, I used [ScanTailor Advanced](https://github.com/4lex4/scantailor-advanced) (specifically commit [`3d1e74e`](https://github.com/4lex4/scantailor-advanced/tree/3d1e74e6ace413733511086934a66f4e3f7a6027).

Except for `Fix orientation`, I did all the steps on each page manually (actually, automatically with manual correction when needed).

The ScanTailor project is located in [`mr2004.scantailor`](/mr2004.scantailor) and the output files in [`st_out/`](/st_out) folder.

## gImageReader

I wanted to use `tesseract` directly, however, the text was less accurately recognised than using gImageReader, therefore I changed my mind and used gImageReader instead. Also as it is a GUI program, proofreading was much easier. Just a note: I used gImageReader GTK v3.3.1 installed from Fedora 31 `@System` repository.

Before the `Initial commit`, I processed 000-006 images with the recognised text saved in [`hocr/mr2004.html`](/hocr/mr2004.html) and [`txt/mr2004.txt`](/txt/mr2004.txt).