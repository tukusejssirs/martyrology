module.exports = {
	env: {
		browser: true,
		node: true,
		es6: true
	},
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	rules: {
		semi: ['error', 'never'],
		quotes: [
			'error',
			'single',
			{
				avoidEscape: true,
				allowTemplateLiterals: true
			}
		],
		'no-unused-vars': ['error', {args: 'none'}],
		'sort-vars': ['error', {ignoreCase: true}],
		'array-bracket-spacing': ['error', 'never'],
		'arrow-body-style': ['error', 'as-needed'],
		'for-direction': 'error',
		'no-trailing-spaces': 'error',
		'comma-dangle': ['error', 'never'],
		'wrap-regex': 'off',
		'no-extra-parens': 'error',
		'no-new-wrappers': 'error',
		'no-mixed-spaces-and-tabs': 'error',
		'no-tabs': ['error', {allowIndentationTabs: true}],
		indent: [
			'error',
			'tab',
			{
				SwitchCase: 1,
				VariableDeclarator: 'first',
				FunctionDeclaration: {parameters: 'first'},
				ArrayExpression: 1,
				ObjectExpression: 1,
				ImportDeclaration: 1,
				flatTernaryExpressions: false
			}
		],
		'max-len': [
			'error',
			{
				code: 9000,
				tabWidth: 2
			}
		],
		'new-cap': ['error', {'newIsCap': false}],
		'no-lonely-if': 'error',
		'computed-property-spacing': ['error', 'never'],
		'object-curly-spacing': ['error', 'never'],
		'array-bracket-spacing': ['error', 'never'],
		'keyword-spacing': ['error', {'before': true, 'after': true}],
		'spaced-comment': ['error', 'always'],
		'no-unreachable': 'error',
		'no-unexpected-multiline': 'error',
		'no-var': 'error'
	}
}