#!/bin/bash

# Format the daily lunar cycle table


# Constants
row_one=(a b c d e f g h i k l m n p q r s t u)
row_three=(A B C D E F F G H M N P)
lunar_month_length=30

# Variables
orig_string="$1"
lunar_day="$2"
row_num=0
done=0

# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
readarray -t orig_array <<< "$(sed 's/<p .*class="[^"]\+/& lunar_cycle_table_day_wrapper/' <<< "$orig_string")"

# Cell values calculation
for i in $(seq 0 "$lunar_month_length"); do
	v=$((lunar_day + i))

	if [ "$v" -gt "$((lunar_month_length))" ]; then
		v=$((v - 30))
	fi

	if [ "${#row_four[@]}" -ge 6 ]; then
		((v--))
	fi

	if [ "${#row_two[@]}" -lt "${#row_one[@]}" ]; then
		readarray -td ' ' row_two < <(printf '%s ' "${row_two[@]}" "$v")
	else
		readarray -td ' ' row_four < <(printf '%s ' "${row_four[@]}" "$v")
	fi
done

for line in "${orig_array[@]}"; do
	span_type="$(grep -Po 'class="\K[^" ]+' <<< "$line")"

	case "$span_type" in
		'ocr_line')
			((row_num++))
			cell_i=-1
			new_line=''
			new_string+=("$line")
		;;
		'ocrx_word')
			((cell_i++))

			# TODO: What if there are more spans than required?
			if [ "${row_one[$cell_i]}" ]; then
				case "$row_num" in
					1)
						new_line="${line//>[^>]*<\/span/>${row_one[$cell_i]}</span}"
						# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
						new_line="$(sed 's|\(<span .*class="[^"]\+\)|\1 lunar_cycle_table_day_19_span|' <<< "$new_line")"
						new_string+=("$(printf '%s\n' "$new_line")")
						line_prev="$new_line"
					;;
					2)
						new_line="${line//>[^>]*<\/span/>${row_two[$cell_i]}</span}"
						# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
						new_line="$(sed 's|\(<span .*class="[^"]\+\)|\1 lunar_cycle_table_day_19_span|' <<< "$new_line")"
						new_string+=("$(printf '%s\n' "$new_line")")
						line_prev="$new_line"
					;;
					3)
						new_line="${line//>[^>]*<\/span/>${row_three[$cell_i]}</span}"

						if [ "$cell_i" = 5 ]; then
							# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
							new_line="$(sed 's|\(<span .*class="[^"]\+\)|\1 lunar_cycle_table_day_12_span red_foreground_colour|' <<< "$new_line")"
						else
							# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
							new_line="$(sed 's|\(<span .*class="[^"]\+\)|\1 lunar_cycle_table_day_12_span|' <<< "$new_line")"
						fi

						new_string+=("$(printf '%s\n' "$new_line")")
						line_prev="$new_line"
					;;
					4)
						new_line="${line//>[^>]*<\/span/>${row_four[$cell_i]}</span}"

						if [ "$cell_i" = 5 ]; then
							# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
							new_line="$(sed 's|\(<span .*class="[^"]\+\)|\1 lunar_cycle_table_day_12_span red_foreground_colour|' <<< "$new_line")"
						else
							# shellcheck disable=SC2001  # See if you can use ${variable//search/replace} instead.
							new_line="$(sed 's|\(<span .*class="[^"]\+\)|\1 lunar_cycle_table_day_12_span|' <<< "$new_line")"
						fi

						new_string+=("$(printf '%s\n' "$new_line")")
						line_prev="$new_line"
					;;
				esac
			fi
		;;
		*)
			case "$row_num" in
				1)
					if [ "$done" -lt 11 ] && [ "$cell_i" -lt "${#row_one[@]}" ]; then
						for ((i = cell_i + 1; i < ${#row_one[@]}; i++)); do
							new_line="$(sed "s|>[^>]*</span|>${row_one[$i]}</span|" <<< "$line_prev")"

							if [ "$new_line" != '' ]; then
								new_string+=("$new_line")
							fi
						done

						if [ "$done" = 10 ]; then
							done=11
						else
							done=10
						fi
					fi
				;;
				2)
					if [ "$done" -lt 22 ] && [ "$cell_i" -lt "${#row_two[@]}" ]; then
						for ((i = cell_i + 1; i < ${#row_two[@]}; i++)); do
							new_line="$(sed "s|>[^>]*</span|>${row_two[$i]}</span|" <<< "$line_prev")"

							if [ "$new_line" != '' ]; then
								new_string+=("$new_line")
							fi
						done

						if [ "$done" = 20 ]; then
							done=22
						else
							done=20
						fi
					fi
				;;
				3)
					if [ "$done" -lt 33 ] && [ "$cell_i" -lt "${#row_three[@]}" ]; then
						for ((i = cell_i + 1; i < ${#row_three[@]}; i++)); do
							new_line="$(sed "s|>[^>]*</span|>${row_three[$i]}</span|" <<< "$line_prev")"

							if [ "$new_line" != '' ]; then
								new_string+=("$new_line")
							fi
						done

						if [ "$done" = 30 ]; then
							done=33
						else
							done=30
						fi
					fi
				;;
				4)
					if [ "$done" -lt 44 ] && [ "$cell_i" -lt "${#row_four[@]}" ]; then
						for ((i = cell_i + 1; i < ${#row_four[@]}; i++)); do
							new_line="$(sed "s|>[^>]*</span|>${row_four[$i]}</span|" <<< "$line_prev")"

							if [ "$i" = 5 ]; then
								new_line="${new_line//lunar_cycle_table_day_12_span/lunar_cycle_table_day_12_span red_foreground_colour}"
							fi

							new_string+=("$new_line")
						done

						done=44
					fi
				;;
			esac

			if grep -vq '<span.*ocrx_word' <<< "$line" && [ "$line" != '' ]; then
				new_string+=("$line")
			fi
	esac
done

# Output the formatted daily lunar cycle table
printf '%s\n' "${new_string[@]}"