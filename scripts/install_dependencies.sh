#!/bin/bash

# Install dependecies that are used to generate HTML files

# Note: The dependencies are currently installed on Debian-like systems only.


# Variables
temp='tmp'
pkgs=(bc dc curl ed jq git unzip w3m zip)

# Create all required folders
rm -rf fonts "$temp"
mkdir fonts "$temp"

echo '[INFO ] Checking dependencies and installing missing dependencies ...' 1>&2

for p in "${pkgs[@]}"; do
	if command -v "$p" &> /dev/null; then
		read -ra pkgs <<< "${pkgs[*]/$p/}"
	fi
done

if [ "${#pkgs[@]}" != 0 ]; then
	if command -v apt-get &> /dev/null; then
		echo "[INFO ] Installing missing dependencies (${pkgs[*]}) ..." 1>&2
		apt-get -y update &> /dev/null
		apt-get -y install "${pkgs[@]}" &> /dev/null
	else
		echo "[ERROR] You need to first install the following dependencies: ${pkgs[*]}" 1>&2
		exit 1
	fi
fi

# Install Go
if [ ! "$(command -v go)" ]; then
	echo '[INFO ] Installing Go ...' 1>&2
	curl -so "$temp/go.tar.gz" https://dl.google.com/go/go1.16.3.linux-amd64.tar.gz
	tar xf "$temp/go.tar.gz" -C .
	GOBIN="$(realpath "go/bin")" go/bin/go get gitlab.com/gitlab-org/release-cli/... &> /dev/null
fi

# Install npm
if [ ! "$(command -v node)" ]; then
	echo '[INFO ] Installing Node JS ...' 1>&2
	curl -sL https://deb.nodesource.com/setup_current.x | bash &> /dev/null
	apt-get -y install nodejs &> /dev/null
fi

# Download Libertinus Serif font
echo '[INFO ] Downloading Libertinus Serif font ...' 1>&2
url_zip="$(curl -s https://api.github.com/repos/alerque/libertinus/releases/latest | jq -r '.assets[] | select(.content_type == "application/zip").browser_download_url')"
curl -sLo "$temp/libertinus.zip" "$url_zip"
unzip -qqj "$temp/libertinus.zip" Libertinus-*/static/OTF/LibertinusSerif-*.otf Libertinus-*/static/WOFF2/LibertinusSerif-*.woff2 -d fonts

# Download and build exsurge
# Note: I needed to disable loading `eslint-loader` in `webpack.config.js`, as it somehow did not like my `.eslintrc.js`.
echo '[INFO ] Downloading and building exsurge ...' 1>&2
git clone -q https://github.com/bbloomf/exsurge.git "$temp/exsurge"
sed -zi 's#\n\s*},\n\s*{\n\s*test:[^,]\+,\n\s*loader: "eslint-loader",\n\s*exclude:[^\n]\+##' "$temp/exsurge/webpack.config.js"
(cd "$temp/exsurge" && npm i &> /dev/null && npm run build &> /dev/null)

# Make changes for both green on black and black on white versions
# TODO: Find a way to scale the score up a bit.

# Copy `exsurge.min.js` (as exsurge_black.min.js`) and `exsurge.min.js.map` to `js` folder
cp "$temp/exsurge/dist/exsurge.min.js" js/exsurge_black.min.js
cp "$temp/exsurge/dist/exsurge.min.js.map" js

# Modify `exsurge.min.js` to output the chant green on black
# TODO: Change foreground colour of clefs, noteheads and accents

# Set the empty note background colour and store the current `exsurge.min.js` version to variable for usage when the green on black version will be generated
# TODO: Change the background colour (`0af`) to black (`000`), after I have found a way to change the foreground colour of noteheads
sed 's/\(fill:"negative"===n\.type[?]"#\).\{3\}/\10af/g;s/define("exsurge"/define("exsurgeGreen"/;s/exports.exsurge=/exports.exsurgeGreen=/;s/t.exsurge/t.exsurgeGreen/' "$temp/exsurge/dist/exsurge.min.js" > js/exsurge_green.min.js

# Install NPM dependencies
npm i

# Remove the temporary folder
rm -rf "$temp"