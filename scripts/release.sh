#!/bin/bash

# Create a new release using data from changelog


# Variables
MARTYROLOGY_HTML="$1"
MARTYROLOGY_TXT="$2"

if [ ! "$MARTYROLOGY_HTML" ] || [ ! "$MARTYROLOGY_TXT" ]; then
	echo '[ERROR] You need to add two arguments with filenames without extensions for archive files, one for HTML files and one for TXT files.' 1>&2
	exit 1
fi

# Get the changelog content
echo '[INFO ] Generating changelog ...' 1>&2
CHANGELOG="$(sed -z 's/^[^|]*//' changelog.md | sed -n '3p')"
COMMIT_CHANGELOG="$(grep -Po '^\|[^\|]*\| \K.*(?= \|$)' <<< "$CHANGELOG" | sed 's/^/- /;s/\\n/\\\\n/g;s/ *<br> *<ul> *<li> */\n   - /g;s/ *<ul> *<li> */\n   - /g;s/ *<\/li> *<li> */;\n   - /g;s/ *<\/li> *<\/ul> *<br> *\([^|]\)/;\n- \1/g;s/ *<\/li> *<\/ul> *\([^|]\)/;\n- \1/g;s| *</li> *</ul> *$||g;s/ *<br> */;\n- /g')."

# Generate progress
echo '[INFO ] Generating progress data ...' 1>&2
total="$(find st_out/*tif | wc -l)"
total_non_empty="$(find hocr/*[0-9]_mr2004.html | wc -l)"
# shellcheck disable=SC2126  # Consider using grep -c instead of grep|wc -l.
done="$(grep '^<!-- done -->' hocr/*[0-9]_mr2004.html | wc -l)"
done_percent="$(bc <<< "scale=3; $done * 100 / $total_non_empty")"
empty_pages=(010 022 028 032 034 038 040 062 070 072 073 074 076 696)
done_pages="$(grep '^<!-- done -->' hocr/*[0-9]_mr2004.html | grep -Po '^hocr/\K[0-9]+')"
done_empty_pages="$(echo -e "$(IFS=$'\n'; echo "${empty_pages[*]}")\n$done_pages" | sort)"

done_empty_pages_range="$(dc -f - -e '
	[ q ]sB
	z d 0 =B sc sa z sb
	[ Sa lb 1 - d sb 0 <Z ]sZ
	lZx
	[ 1 sk lf 1 =O lk 1 =M ]sS
	[ li p c 0 d sk sf ]sO
	[ 2 sf lh d sj li 1 + !=O ]sQ
	[ li n [-] n lj p c 0 sf ]sM
	[ 0 sk lh sj ]sN
	[ 1 sk lj lh 1 - =N lk 1 =M ]sR
	[ 1 sf lh si ]sP
	[ La sh lc 1 - sc lf 2 =R lf 1 =Q lf 0 =P lc 0 !=A ]sA
	lAx
	lSx' <<< "$done_empty_pages" | sed -z 's/\n/, /g;s/, $//')"

# Create release description
RELEASE_CHANGELOG="$(echo -e "$COMMIT_CHANGELOG\n\n\`\`\`js\ntotal number of pages  : $total\nnon-empty pages number : $total_non_empty\nprogress               : $done done of $total_non_empty ($done_percent %)\nlist of pages done     : $done_empty_pages_range\n\`\`\` ")"

if [ -d html ] && [ -d fonts ] && [ -d txt ]; then
	echo '[INFO ] Creating asset archives ...' 1>&2
	zip -rqq "$MARTYROLOGY_HTML.zip" css fonts html img js index.html
	zip -rqq "$MARTYROLOGY_TXT.zip" txt
else
	echo '[ERROR] You need to generate both HTML and TXT files.' 1>&2
	# shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
	echo '        Try again after you generate the files using `npm run gen:all`.' 1>&2
fi

# Create new release on GitLab
echo '[INFO ] Creating a new release ...' 1>&2
GOBIN="$(realpath "go/bin")" "go/bin/release-cli" --server-url https://gitlab.com create --name "Release $CI_COMMIT_TAG" --tag-name "$CI_COMMIT_TAG" --description "$RELEASE_CHANGELOG" --assets-link "{\"name\": \"$MARTYROLOGY_HTML.zip\", \"url\": \"$JOBS_URL/$MARTYROLOGY_HTML.zip\"}" --assets-link "{\"name\": \"$MARTYROLOGY_TXT.zip\", \"url\": \"$JOBS_URL/$MARTYROLOGY_TXT.zip\"}"

# Remove Go and `release-cli`
rm -rf go