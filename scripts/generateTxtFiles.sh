#!/bin/bash

# Build the text file version of Martyrologium Romanum (2004)


echo '[INFO ] Generating TXT files ...' 1>&2
mkdir txt

for f in html/*.html; do
	w3m -I 'UTF-8' -O 'UTF-8' -dump -W -T text/html "$f" | sed -z 's/^.*\nBack Cover\n\n//' > txt/"$(basename "$f" .html)".txt
done