#!/usr/bin/env node

'use strict'

const {close, existsSync, mkdirSync, open, openSync, read, statSync, write} = require('fs')
const {sync} = require('glob')
const {JSDOM} = require('jsdom')
const {resolve} = require('path')
const {each} = require('async')

main()

/**
 * Generate HTML files of Martyrologium Romanum (2004)
 */
async function main() {
	// File buffer sizes in bytes
	const MAIN_BUFF_SIZE = 10000000
	const PART_BUFF_SIZE = 1000000

	// Metadata of sections of MR 2004 are used to group pages into blocks
	const sections = {
		frontMatter: {name: '01_front_matter.html', startPage: 0, endPage: 4, removeIds: []},
		decrees: {name: '02_decrees.html', startPage: 5, endPage: 8, removeIds: []},
		introduction: {name: '03_introduction.html', startPage: 9, endPage: 21, removeIds: []},
		lunarDayPronunciation: {name: '04_lunar_day_pronunciation.html', startPage: 23, endPage: 26, removeIds: []},
		orderOfLessions: {name: '05_order_of_lessons.html', startPage: 27, endPage: 31, removeIds: []},
		eulogiesForMovableCelebrations: {name: '06_eulogies_for_movable_celebrations.html', startPage: 33, endPage: 37, removeIds: []},
		shortReadings: {name: '07_short_readings.html', startPage: 39, endPage: 60, removeIds: []},
		prayers: {name: '08_prayers.html', startPage: 61, endPage: 68, removeIds: []},
		singingOfTheMartyrology: {name: '09_singing_of_the_martyrology.html', startPage: 69, endPage: 74, removeIds: []},
		romanMartyrologyJanuary: {name: '10_roman_martyrology_january.html', startPage: 75, endPage: 126, removeIds: []},
		romanMartyrologyFebruary: {name: '11_roman_martyrology_february.html', startPage: 127, endPage: 170, removeIds: []},
		romanMartyrologyMarch: {name: '12_roman_martyrology_march.html', startPage: 171, endPage: 213, removeIds: []},
		romanMartyrologyApril: {name: '13_roman_martyrology_april.html', startPage: 214, endPage: 262, removeIds: []},
		romanMartyrologyMay: {name: '14_roman_martyrology_may.html', startPage: 263, endPage: 314, removeIds: []},
		romanMartyrologyJune: {name: '15_roman_martyrology_june.html', startPage: 315, endPage: 362, removeIds: []},
		romanMartyrologyJuly: {name: '16_roman_martyrology_july.html', startPage: 363, endPage: 426, removeIds: []},
		romanMartyrologyAugust: {name: '17_roman_martyrology_august.html', startPage: 427, endPage: 488, removeIds: []},
		romanMartyrologySeptember: {name: '18_roman_martyrology_september.html', startPage: 489, endPage: 547, removeIds: []},
		romanMartyrologyOctober: {name: '19_roman_martyrology_october.html', startPage: 548, endPage: 599, removeIds: []},
		romanMartyrologyNovember: {name: '20_roman_martyrology_november.html', startPage: 600, endPage: 651, removeIds: []},
		romanMartyrologyDecember: {name: '21_roman_martyrology_december.html', startPage: 652, endPage: 694, removeIds: []},
		romanMartyrologyNameIndexA: {
			name: '22_name_index_a.html', startPage: 695, endPage: 709,
			removeIds: [
				'carea_706_7',
				'carea_706_8'
			]
		},
		nameIndexB: {
			name: '23_name_index_b.html', startPage: 709, endPage: 717,
			removeIds: [
				'carea_706_2',
				'carea_706_4',
				'carea_706_6',
				'carea_714_7',
				'carea_714_8'
			]
		},
		nameIndexC: {
			name: '24_name_index_c.html', startPage: 717, endPage: 727,
			removeIds: [
				'carea_714_1',
				'carea_714_2',
				'carea_714_4',
				'carea_714_5',
				'carea_724_9',
				'carea_724_10',
				'carea_724_11'
			]
		},
		nameIndexD: {
			name: '25_name_index_d.html', startPage: 727, endPage: 734,
			removeIds: [
				'carea_724_1',
				'carea_724_2',
				'carea_724_4',
				'carea_724_5',
				'carea_724_6',
				'carea_724_8',
				'carea_731_5',
				'carea_731_6',
				'carea_731_7',
				'carea_731_9'
			]
		},
		nameIndexE: {
			name: '26_name_index_e.html', startPage: 734, endPage: 739,
			removeIds: [
				'carea_731_4',
				'carea_736_5',
				'carea_736_6',
				'carea_736_8'
			]
		},
		nameIndexF: {
			name: '27_name_index_f.html', startPage: 739, endPage: 746,
			removeIds: [
				'carea_736_4',
				'carea_743_7',
				'carea_743_8',
				'carea_743_10',
				'carea_743_11'
			]
		},
		nameIndexG: {
			name: '28_name_index_g.html', startPage: 746, endPage: 753,
			removeIds: [
				'carea_743_4',
				'carea_743_5',
				'carea_743_6'
			]
		},
		nameIndexH: {
			name: '29_name_index_h.html', startPage: 754, endPage: 758,
			removeIds: [
				'carea_755_8',
				'carea_755_9'
			]
		},
		nameIndexI: {
			name: '30_name_index_i.html', startPage: 758, endPage: 771,
			removeIds: [
				'carea_755_4',
				'carea_755_6',
				'carea_755_7',
				'carea_768_7',
				'carea_768_8',
				'carea_768_9'
			]
		},
		nameIndexJ: {
			name: '31_name_index_j.html', startPage: 771, endPage: 772,
			removeIds: [
				'carea_768_1',
				'carea_768_2',
				'carea_768_4',
				'carea_768_6',
				'carea_769_7',
				'carea_769_8'
			]
		},
		nameIndexK: {
			name: '32_name_index_k.html', startPage: 772, endPage: 774,
			removeIds: [
				'carea_769_1',
				'carea_769_2',
				'carea_769_4',
				'carea_769_6',
				'carea_771_5',
				'carea_771_6',
				'carea_771_8',
				'carea_771_9',
				'carea_771_10'
			]
		},
		nameIndexL: {
			name: '33_name_index_l.html', startPage: 774, endPage: 781,
			removeIds: [
				'carea_771_1',
				'carea_771_2',
				'carea_771_4',
				'carea_778_9',
				'carea_778_10',
				'carea_778_11'
			]
		},
		nameIndexM: {
			name: '34_name_index_m.html', startPage: 781, endPage: 798,
			removeIds: [
				'carea_778_1',
				'carea_778_2',
				'carea_778_4',
				'carea_778_6',
				'carea_778_7',
				'carea_778_8',
				'carea_795_8',
				'carea_795_9'
			]
		},
		nameIndexN: {
			name: '35_name_index_n.html', startPage: 798, endPage: 802,
			removeIds: [
				'carea_795_1',
				'carea_795_2',
				'carea_795_4',
				'carea_795_6',
				'carea_795_7',
				'carea_799_5',
				'carea_799_6',
				'carea_799_8'
			]
		},
		nameIndexO: {
			name: '36_name_index_o.html', startPage: 802, endPage: 803,
			removeIds: [
				'carea_799_1',
				'carea_799_2',
				'carea_799_4',
				'carea_800_7',
				'carea_800_8'
			]
		},
		nameIndexP: {
			name: '37_name_index_p.html', startPage: 803, endPage: 815,
			removeIds: [
				'carea_800_1',
				'carea_800_2',
				'carea_800_4',
				'carea_800_6',
				'carea_812_9',
				'carea_812_5',
				'carea_812_6',
				'carea_812_8',
				'carea_812_10',
				'carea_812_11',
				'carea_812_12',
				'carea_812_13'
			]
		},
		nameIndexQ: {
			name: '38_name_index_q.html', startPage: 815, endPage: 815,
			removeIds: [
				'carea_812_1',
				'carea_812_2',
				'carea_812_4',
				'carea_812_9',
				'carea_812_10',
				'carea_812_11',
				'carea_812_12',
				'carea_812_13'
			]
		},
		nameIndexR: {
			name: '39_name_index_r.html', startPage: 815, endPage: 821,
			removeIds: [
				'carea_812_1',
				'carea_812_2',
				'carea_812_4',
				'carea_812_5',
				'carea_812_6',
				'carea_812_8',
				'carea_818_8',
				'carea_818_9'
			]
		},
		nameIndexS: {
			name: '40_name_index_s.html', startPage: 821, endPage: 830,
			removeIds: [
				'carea_818_1',
				'carea_818_2',
				'carea_818_4',
				'carea_818_5',
				'carea_818_7',
				'carea_827_5',
				'carea_827_6',
				'carea_827_8',
				'carea_827_9'
			]
		},
		nameIndexT: {
			name: '41_name_index_t.html', startPage: 830, endPage: 836,
			removeIds: [
				'carea_827_1',
				'carea_827_2',
				'carea_827_4',
				'carea_833_5',
				'carea_833_6',
				'carea_833_8'
			]
		},
		nameIndexU: {
			name: '42_name_index_u.html', startPage: 836, endPage: 836,
			removeIds: [
				'carea_833_1',
				'carea_833_2',
				'carea_833_4'
			]
		},
		nameIndexV: {
			name: '43_name_index_v.html', startPage: 837, endPage: 841,
			removeIds: [
				'carea_838_6',
				'carea_838_7',
				'carea_838_9'
			]
		},
		nameIndexW: {
			name: '44_name_index_w.html', startPage: 841, endPage: 842,
			removeIds: [
				'carea_838_1',
				'carea_838_2',
				'carea_838_4',
				'carea_838_5',
				'carea_839_9',
				'carea_839_10',
				'carea_839_11',
				'carea_839_12'
			]
		},
		nameIndexX: {
			name: '45_name_index_x.html', startPage: 842, endPage: 842,
			removeIds: [
				'carea_839_1',
				'carea_839_2',
				'carea_839_4',
				'carea_839_5',
				'carea_839_6',
				'carea_839_8',
				'carea_839_11',
				'carea_839_12'
			]
		},
		nameIndexY: {
			name: '46_name_index_y.html', startPage: 842, endPage: 843,
			removeIds: [
				'carea_839_1',
				'carea_839_2',
				'carea_839_4',
				'carea_839_5',
				'carea_839_6',
				'carea_839_8',
				'carea_839_9',
				'carea_839_10',
				'carea_840_7',
				'carea_840_8'
			]
		},
		nameIndexZ: {
			name: '47_name_index_z.html', startPage: 843, endPage: 844,
			removeIds: [
				'carea_840_1',
				'carea_840_2',
				'carea_840_4',
				'carea_840_6'
			]
		},
		backMatter: {name: '48_back_matter.html', startPage: 845, endPage: 999, removeIds: []}
	}

	const partial = resolve(__dirname, '../partials/template.html')
	const partialContent = await getFileContent(partial, PART_BUFF_SIZE)
	const outputPath = resolve(__dirname, '../html')
	const pattern = resolve(__dirname, '../hocr/*html')
	const files = sync(pattern)

	// Make sure output path exists
	if (! existsSync(outputPath)){
		mkdirSync(outputPath)
	}

	generateHtmlFiles(files, sections, partialContent, MAIN_BUFF_SIZE, outputPath)

	createIndexHtml()
}

/**
 * Generate HTML files of Martyrologium Romanum (2004)
 *
 * @param      {string[]}  files        Array of all files (a page per file)
 * @param      {object}    sections     Object with metadata of all sections
 * @param      {string}    partial      Patial template content
 * @param      {string}    outputPath   Output path
 * @return     {Promise}   If all files were successfully generated
 */
async function generateHtmlFiles(files, sections, partial, size, outputPath) {
	each(Object.keys(sections), async section => {
		let file = outputPath + '/' + sections[section].name
		let buffer = Buffer.from(await getSectionContent(files, sections[section].startPage, sections[section].endPage, 3, '', '_mr2004.html$', partial, section, size, sections), 'utf-8')

		open(file, 'w', function(err, fd) {
			if (err) {
				callback(err)
				console.log('[ERROR] ' + new Date().toISOString() + ' : Could not open the following file:', err)
			}

			// Write the contents of the buffer, from position 0 to the end, to the file descriptor returned in opening our file
			write(fd, buffer, 0, buffer.length, null, function(err, bytesWritten) {
				if (err) {
					console.log('[ERROR]  ' + new Date().toISOString() + ' : An error occured while writing the file:', err)
					callback(err)
				} else {
					close(fd, function() {
						console.log('[INFO]  ' + new Date().toISOString() + ' : File ' + sections[section].name + ' was successfully generated. Number of bytes written: ' + bytesWritten + ' B.')
					})
				}
			})
		})
	}, (err) => {
		if (err) {
			console.log('[WARN]  ' + new Date().toISOString() + ' : At least one file was not generated successfully.\n', err)
		} else {
			console.log('[INFO]  ' + new Date().toISOString() + ' : All files were successfully generated.')
		}
	})
}

/**
 * Create section content from the section files
 *
 * @param      {string[]}  files     Array of all files (a page per file)
 * @param      {number}    start     Start page
 * @param      {number}    end       End page
 * @param      {number}    width     Number width in the regex
 * @param      {string}    prefix    Regex prefix
 * @param      {string}    suffix    Regex suffix
 * @param      {string}    partial   Patial template content
 * @param      {string}    section   Section name which is to be processed
 * @param      {Object}    size      Buffer size
 * @param      {Object}    sections  Sections metadata
 */
async function getSectionContent(files, start, end, width, prefix, suffix, partial, section, size, sections) {
	// Create regex to match all pages in the section
	let regex = new RegExp(parseNumRangeToRegex(start, end, width, prefix, suffix))

	// Get a list of pages in a section
	let sectionFiles = files.filter(file => regex.test(file))
	let domPartial = new JSDOM(partial).window.document

	for (const file of sectionFiles) {
		let content = await getFileContent(file, size).catch(err => console.log('[ERROR] ' + new Date().toISOString() + ' : There was error in generating the following file: ' + file + '.\n', err))
		let bodyPartial = domPartial.querySelector('body').innerHTML.replace(/\n*$/, '')
		let body = new JSDOM(content).window.document.querySelector('body').innerHTML.replace(/^[\n]*/, '').replace(/\n*$/, '')
		domPartial.querySelector('body').innerHTML = bodyPartial + '\n' + body
		content = ''
		bodyPartial = ''
		body = ''
	}

	// We need to remove some elements (`<div>` tags) in order to keep only entries that belong to the particular letter
	each(sections[section].removeIds, async removeId => {
		domPartial.getElementById(removeId).remove()
	}, err => {
		if (err) {
			console.log('[WARN]  ' + new Date().toISOString() + ' : An error occured while removing an ID.\n', err)
		}
	})

	return domPartial.querySelector('html').outerHTML
		.replace(/^(<html lang="la" class="scrollbar">)/, '<!DOCTYPE html>\n\n$1\n\n')
		.replace(/<\/body><\/html>$/, '\n</body>\n\n</html>')
}

/**
 * Parse number range to regex
 *
 * src: https://stackoverflow.com/a/67319250/3408342
 *
 * @param      {number}   min          Range start
 * @param      {number}   max          Range end
 * @param      {number}   [width=0]    Number width in the regex
 * @param      {string}   [prefix='']  Regex prefix
 * @param      {string}   [suffix='']  Regex suffix
 */
function parseNumRangeToRegex(min, max, width = 0, prefix = '', suffix = '') {
	if (! Number.isInteger(min) || ! Number.isInteger(max) || min > max || min < 0 || max < 0) {
		return false
	}

	if (min == max) {
		return parseIntoPattern(min, prefix, suffix)
	}

	let x = parseStartRange(min, max)
	let s = []

	x.forEach(o => {
		s.push(parseEndRange(o[0], o[1]))
	})

	let n = reformatArray(s)
	let h = parseIntoRegex(n, width)

	return parseIntoPattern(h, prefix, suffix)
}

/**
 * Parse the regex array to regex string
 *
 * @param      {string[]}  regex        Regex array type 2
 * @param      {string}    [prefix='']  Regex prefix
 * @param      {string}    [suffix='']  Regex suffix
 * @return     {string}    Regex string
 */
function parseIntoPattern(regex, prefix = '', suffix = '') {
	let r = Array.isArray(regex) ? regex.join('|') : regex
	return prefix + '(' + r + ')' + suffix
}

/**
 * Parse the regex array to regex
 *
 * @param      {string[]}  t          Regex array type 1
 * @param      {number}    [width=0]  Number width in the regex
 * @return     {string[]}  Regex array
 */
function parseIntoRegex(t, width = 0) {
	if (! Array.isArray(t)) {
		throw new Error('[ERROR] Argument needs to be an array!')
	}

	let r = []

	for (let i = 0; i < t.length; i++) {
		let e = t[i][0].split('')
		let n = t[i][1].split('')
		let s = ''
		let o = 0
		let h = ''

		for (let a = 0; a < e.length; a++) {
			if (e[a] === n[a]) {
				h += e[a]
			} else if (parseInt(e[a]) + 1 === parseInt(n[a])) {
				h += '[' + e[a] + n[a] + ']'
			} else {
				if (s === e[a] + n[a]) {
					o++
				}

				s = e[a] + n[a]

				if (a == e.length - 1) {
					h += o > 0 ? '{' + (o + 1) + '}' : '[' + e[a] + '-' + n[a] + ']'
				} else if (o === 0) {
					h += '[' + e[a] + '-' + n[a] + ']'
				}
			}
		}

		if (e.length < width) {
			h = '0'.repeat(width - e.length, '0') + h
		}

		r.push(h)
	}

	return r
}

/**
 * Auxiliary function to reformat the array of subrange intervals
 *
 * @param      {string[]}  t       Array of subrange intervals
 * @return     {string[]}  Reformatted array of subrange intervals
 */
function reformatArray(t) {
	let arrReturn = []

	for (let i = 0; i < t.length; i++) {
		let page = t[i].length / 2

		for (let a = 0; a < page; a++) {
			arrReturn.push(t[i].slice(2 * a))
		}
	}

	return arrReturn
}

/**
 * Get start of subrange intervals
 *
 * @param      {string}  t       Range start
 * @param      {string}  r       Range end
 * @return     {string[]}   Array of start of subrange intervals
 */
function parseStartRange(t, r) {
	t = t.toString()
	r = r.toString()

	if (t.length === r.length) {
		return [[t, r]]
	}

	let breakOut = 10 ** t.length - 1
	return [[t, breakOut.toString()]].concat(parseStartRange(breakOut + 1, r))
}

/**
 * Get end of subrange intervals
 *
 * @param      {string}  t       Range start
 * @param      {string}  r       Range end
 * @return     {string[]}   Array of end of subrange intervals
 */
function parseEndRange(t, r) {
	if (t.length == 1) {
		return [t, r]
	}

	if ('0'.repeat(t.length) === '0' + t.substr(1)) {
		if ('0'.repeat(r.length) == '9' + r.substr(1)) {
			return [t, r]
		}

		if (parseInt(t.toString().substr(0, 1)) < parseInt(r.toString().substr(0, 1))) {
			let e = parseInt(r.toString().substr(0, 1) + '0'.repeat(r.length - 1)) - 1
			return [t, strBreakPoint(e)].concat(parseEndRange(strBreakPoint(e + 1), r))
		}
	}

	if ('9'.repeat(r.length) === '9' + r.toString().substr(1) && parseInt(t.toString().substr(0, 1)) < parseInt(r.toString().substr(0, 1))) {
		let e = parseInt(parseInt(parseInt(t.toString().substr(0, 1)) + 1) + '0'.repeat(r.length - 1)) - 1
		return parseEndRange(t, strBreakPoint(e)).concat(strBreakPoint(e + 1), r)
	}

	if (parseInt(t.toString().substr(0, 1)) < parseInt(r.toString().substr(0, 1))) {
		let e = parseInt(parseInt(parseInt(t.toString().substr(0, 1)) + 1) + '0'.repeat(r.length - 1)) - 1
		return parseEndRange(t, strBreakPoint(e)).concat(parseEndRange(strBreakPoint(e + 1), r))
	}

	let a = parseInt(t.toString().substr(0, 1))
	let o = parseEndRange(t.toString().substr(1), r.toString().substr(1))
	let h = []

	for (let u = 0; u < o.length; u++) {
		h.push(a + o[u])
	}

	return h
}

/**
 * Pad a number string
 *
 * @param      {string}  t       Number
 * @return     {string}  Padded number
 */
function strBreakPoint(t) {
	return t.toString().padStart((parseInt(t) + 1).toString().length, '0')
}

/**
 * Get file content
 *
 * @param      {string}   file    Filename
 * @param      {number}   size    Buffer size
 * @return     {Promise}  The file content
 */
async function getFileContent(file, size) {
	for await (const chunk of generateChunks(file, size)) {
		return chunk.toString('utf8')
	}
}

/**
 * Read bytes from the buffer
 *
 * @param      {number}   fd            File descriptor
 * @param      {Buffer}   sharedBuffer  Buffer
 * @return     {Promise}  If the bytes where successfully read
 */
function readBytes(fd, sharedBuffer) {
	return new Promise((resolve, reject) => {
		read(
			fd,
			sharedBuffer,
			0,
			sharedBuffer.length,
			null,
			(err) => {
				if (err) {
					return reject(err)
				}

				resolve()
			}
		)
	})
}

/**
 * Iteratee that generates chunks of data read from file
 *
 * @param      {string}   file  Filename
 * @param      {number}   size  Buffer size
 * @return     {Promise}  { description_of_the_return_value }
 */
async function* generateChunks(file, size) {
	const sharedBuffer = Buffer.alloc(size)
	const stats = statSync(file)
	const fd = openSync(file)
	let bytesRead = 0
	let end = size

	for (let i = 0; i < Math.ceil(stats.size / size); i++) {
		await readBytes(fd, sharedBuffer)
		bytesRead = (i + 1) * size

		if (bytesRead > stats.size) {
			// When we reach the end of file, we have to calculate how many bytes were actually read
			end = size - (bytesRead - stats.size)
		}

		yield sharedBuffer.slice(0, end)
	}
}

/**
 * Create index.html file
 *
 * @return     {Promise}  If the file was successfully created
 */
async function createIndexHtml() {
	const content = '<!DOCTYPE html>\n\n<html lang="la">\n\n<head>\n\t<title>Martyrologium Romanum, editio altera (2004)</title>\n\t<meta http-equiv="refresh" content="2; url = html/01_front_matter.html" />\n</head>\n\n</html>'

	const file = 'index.html'
	const buffer = Buffer.from(content, 'utf-8')

	open(resolve(__dirname, '../' + file), 'w', function(err, fd) {
		if (err) {
			console.log('[ERROR] ' + new Date().toISOString() + ' : Could not open the following file:', err)
		}

		// Write the contents of the buffer, from position 0 to the end, to the file descriptor returned in opening our file
		write(fd, buffer, 0, buffer.length, null, function(err, bytesWritten) {
			if (err) {
				console.log('[ERROR]  ' + new Date().toISOString() + ' : An error occured while writing the file:', err)
			} else {
				close(fd, function() {
					console.log('[INFO]  ' + new Date().toISOString() + ' : File ' + file + ' was successfully generated. Number of bytes written: ' + bytesWritten + ' B.')
				})
			}
		})
	})
}