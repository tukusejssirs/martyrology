# Errata

I have found the following potential errors in _Martyrologium Romanum_, ed. altera (2004). Note that as for now, only I see them as issues; they need to be verified by the editors of Libreria Editrice Vaticana.

Also note that these errors can still be found in the `mr2004.html` and `mr2004.txt`.

- general typos:
   - p042: _In festo sancti Ioannis, Apostoli et Evangelistæ_: there is an extra space between the hyphen and number five in `Eccli 15, 1-5`;
   - p046: _Feria V vel Dominica post Ss. mam Trinitatem: SS. mi Corporis et Sanguinis Christi_: there is an extra space between `SS.` and `mi`;
   - p082: `RetiáriæRatiáriæ` → `Retiáriæ` (4 January, 1st eulogy);
   - p222: `Velerádii` → `Velehrádii` (6 April, 6th eulogy);
   - p389: `Stáry Kynsperk` → `Starý Kynšperk` (14 July, 5th eulogy);
   - p397: `Gojdich` → `Gojdič` (17 July, 16th eulogy);
   - p408: `Makhlūūf` → `Makhlūf` (24 July, 1st eulogy);
   - p413: `Maríæ Terésiæ Kowalska` → `Maríæ Terésiæ ab Iesu Infante (Miecislavæ) Kowalska` (25 July, 20th eulogy).
- style-related issues:
   - (many places): When referencing _Missale Romanum_, _Liturgia Horarum_ and _Codex Iuris Canonici_ (in citations), it is always styled as an author (i.e. in small caps); should it not be rather in italics as the source (book) title?
   - (many places): should not `ae` (when pronounced as `[eː]`) always be replaced with `æ` ligature? (e.g. `praefatio` → `præfatio`)
   - p013: footnote 16: should not whole _Preces in sollemnitate Omnium Sanctorum_ be in italics?
   - p052 (30., line 1), p054 (35., line 1): should not `gloria` in the text of prayers / Scriptures _always_ have an accute accent (i.e. `glória`)? if so, here is a typo;
   - p057: should not the `“”` quotes be replaced by angle quotes (`«»`)?
   - p059: should not the `aut` after a question mark have `A` in uppercase (as a sentence start)?
   - p109: missing full stop (`.`) at the end of the eulogy (21 January, 2nd eulogy);
   - [many places]: should not `quorum` in the text of prayers / Scriptures _always_ have an accute accent (i.e. `quórum`)? currently, sometimes there is an acute accent, often there is none.