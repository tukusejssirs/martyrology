# Martyrology

Convert hard-copy, printed edition of Martyrologium Romanum, ed. altera (2004) to an OCR’ed version.

## About

The CSS style of [`hocr/mr2004.html`](/hocr/mr2004.html) is opiniated. It is based on the printed style, but I did not try to mimic it faithfully. It is styled as green text on black background with red rubrics. If you want to change text text and background colours, you can do it by change the colour in the `normal_foreground_colour` (and the border colour in `temporary_table_cell` class; i.e. all `#00aa00` instances to the colour of your choice) and `normal_background_colour` classes. I’d like to emphasis that it certainly is far from perfect and any contribution in this area is most welcome. :smiley:

## Changelog

Changelog can be found either as commit history (using `git log`) or in [`changelog.md`](/changelog.md) file.

Note that I list what I did before versioned the data, is listed in [`processing_book.md`](/processing_book.md).

## TODO list

Here is a list of what I’d like to do before I release version `1.0.0`. Note that the list is not in any particular order, although some tasks depend on others. Once a task is done, it’ll be removed from this list.

- [x] OCR the whole book;
- [ ] proofread the text of the whole book;
- [ ] style the HTML:
      - [ ] add CSS style to make that file to improve the look of `hocr/mr2004.html` in browser;
		- [ ] TOC (p845) should contain links to the respective headings;
      - [ ] add bibliography (referenced by MR2004);
      - [ ] add links to the cited documents which are available online;
		- [ ] create links to footnotes between footnote references or even better: create real footnotes (or maybe even pop-ups/tooltips like Wikipedia does; this will be done after proofreading and styling entire MR2004);
      - [ ] 071 - [exsurge] remove Junicode font and use Libertinus Serif after `oé` ligature is fixed;
      - [ ] 071 - [exsurge] find a way to scale the score up a bit (alternatively, modify [`css/mr2004.css`](css/mr2004.css) to use small fonts);
      - [ ] every day of year and every eulogy on any day should be directly linkable (by ID);
      - [ ] `scripts/generateHtmlFiles.js`: consider removing all unnecessary elements like `title=".*"` or `id="carea_52_3` or header (e.g. book title and page number);
- [ ] localise:
   - [ ] the values os `<img>` tag `alt` attributes;
   - [ ] `partials/template.html`:
      - [ ] `front matter`: `pars præliminaris` or rather `præliminaria`?
      - [ ] `front cover`;
      - [ ] `half title`;
      - [ ] `back matter`: `pars postliminaris`?
      - [ ] `back cover`;
- [ ] create PDF file with images and hOCR file embedded;
- [ ] create a LaTeX document with the processed data with all corrections of all the issues I find [this is of low priority].

## Contributing

The task of converting a book into an OCR’ed document takes some time. If you want to help me, read the [`contributing.md`](/contributing.md) file on the rules I set myself.

Note that I manually proofread the OCR’ed text and while I did my best to make the text as faithful to the printed book, I might either missed some typos or caused some errors. Please create a PR or raise an issue if you find any error. Thanks!

## Licence

The text itself is owned by [Libreria Editrice Vaticana](https://www.libreriaeditricevaticana.va/), the publishing house of the Holy See.

The work I do here is licenced under GPLv3 which text is in [`licence.md`](/licence.md).