const menu = document.querySelector('.menu')
const hamburgerId = document.querySelector('.hamburgerId')

hamburgerId.addEventListener('click', toggleMenu)

function toggleMenu() {
	hamburgerId.classList.toggle('close')

	if (menu.classList.contains('showMenu')) {
		menu.classList.remove('showMenu')
	} else {
		menu.classList.add('showMenu')
	}
}
