function setTheme(theme) {
	const classes = [
		'body',
		'.border_thick_right',
		'.hamburger',
		'.hamburgerId',
		'.heading_3_alt',
		'.large_colour',
		'.menu',
		'.menu_link_colour',
		'.menu_section_content',
		'.menu_section_label',
		'.menu_section_label_no_content',
		'.menu_subsection_content_colour',
		'.menu_subsection_label_colour',
		'.menuLink',
		'.normal_foreground_colour',
		'.scrollbar',
		'.small_colour',
		'.temporary_table_cell',
		'.theme_toggle_colour',
		'.title_page_coat_of_arms_image_colour'
	]

	if (theme == 'black') {
		classes.forEach(c => document.querySelectorAll(c).forEach(key => key.classList.add('black_theme')))
		document.querySelector('input').checked = true
		document.querySelector('.theme_toggle').classList.add('move')

		try {
			appendParamsToAllUrls('black')
		} catch (err) {
			// Do nothing
		}
	} else {
		classes.forEach(c => document.querySelectorAll(c).forEach(key => key.classList.remove('black_theme')))
		document.querySelector('input').checked = false
		document.querySelector('.theme_toggle').classList.remove('move')

		try {
			appendParamsToAllUrls('green')
		} catch (err) {
			// Do nothing
		}
	}
}

function toggleTheme() {
	if (getParam('theme') == 'black') {
		setParam('theme', 'green')
		setTheme('green')
		document.querySelector('input').checked = false
		document.querySelector('.theme_toggle').classList.remove('move')

		try {
			updateChant('green')
		} catch (err) {
			// Do nothing
		}
	} else {
		setParam('theme', 'black')
		setTheme('black')
		document.querySelector('input').checked = true
		document.querySelector('.theme_toggle').classList.add('move')

		try {
			updateChant('black')
		} catch (err) {
			// Do nothing
		}
	}
}

function setParam(key, value) {
	const urlParams = new URLSearchParams(document.location.search)
	urlParams.set(key, value)
	let params = '?' + urlParams.toString()
	window.history.pushState('', document.title, params)
}

function getParam(key) {
	return new URLSearchParams(document.location.search).get(key)
}

function appendParamsToAllUrls(theme) {
	const links = document.querySelectorAll('a')

	links.forEach(link => {
		const href = link.href

		if (href) {
			const url = new URL(href)
			url.searchParams.set('theme', theme)
			link.href = url.href
		}
	})
}
