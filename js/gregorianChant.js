let ex = {
	black: exsurge,
	green: exsurgeGreen
}

let theme = getParam('theme')

// Variables
let gabcSourceElMar = '(c3) Quar(h)to(h) No(h)nas(h) ia((h)nu(h)á(h)ri(h)i.(h) Lu(h)na(h) ....(h).....(hr1)...(dr)...(d:) _To(h)_nus_(h) _sic_(h) _fléc_(hr1)_ti_(dr)_tur,_(d,) † _et_(h) _sic_(h) _fi_(h)_ní_(h dr)_tur._(d:) _Pe_(h)_rí_(h)_o_(h)_dus_(h) _fi_(h)_ná_(h)_lis_(h,) _lec_(h)_ti_(h)_ó_(h)_nem_(h) _sic_(h) _con_(h)_clú_(hr1 dr)_dit._(d::)'
let gabcSourceElNat = '(c3) Oc(h)tá(h)vo(h) Ka(h)lén(h)das(h) ia(h)nu(h)á(h)ri(h)i.(h) Lu(h)na(h) ....(h).....(hr1)...(dr)...(d:) In(h)nú(h)me(h)ris(h) tran(h)sác(h)tis(h) sǽ(h)cu(h)lis(h) a(h) cre(h)a(h)ti(h)ó(h)ne(h) mun(h)di,(h,) quan(h)do(h) in(h) prin(h)cí(h)pi(h)o(h) De(h)us(h) cre(h)á(h)vit(h) cæ(h)lum(h) et(h) ter(h)ram(h,) et(h) hó(h)mi(h)nem(h) for(h)má(h)vit(h) ad(h) i(h)má(h)gi(h)nem(h) su(h)am;(d:) per(h)múl(h)tis(h) é(h)ti(h)am(h) sǽ(h)cu(h)lis,(h) ex(h) quo(h) post(h) di(h)lú(h)vi(h)um(h,) Al(h)tís(h)si(h)mus(h) in(h) nú(h)bi(h)bus(h) ar(h)cum(h) po(h)sú(h)e(h)rat,(h) sig(h)num(h) fœ́(h)de(h)ris(h) et(h) pa(h)cis;(d:) a(h) mi(h)gra(h)ti(h)ó(h)ne(h) A(h)bra(h)hæ,(h) pa(h)tris(h) nos(h)tri(h) in(h) fi(h)de,(h) de(h) Ur(h) Chal(h)dæ(h)ó(h)rum(h,) sǽ(h)cu(h)lo(h) vi(h)gé(h)si(h)mo(h) pri(h)mo;(d:) ab(h) e(h)grés(h)su(h) pó(h)pu(h)li(h) Is(h)ra(h)el(h) de(h) Æ(h)gýp(h)to,(h) Mó(h)y(h)se(h) du(h)ce,(h,) sǽ(h)cu(h)lo(h) dé(h)ci(h)mo(h) tér(h)ti(d)o;(d:) ab(h) unc(h)ti(h)ó(h)ne(h) Da(h)vid(h) in(h) re(h)gem(h,) an(h)no(h) cír(h)ci(h)ter(h) mil(h)lé(h)si(d)mo,(d:) heb(h)dó(h)ma(h)da(h) se(h)xa(h)gé(h)si(h)ma(h) quin(h)ta,(h,) iux(h)ta(h) Da(h)nié(h)lis(h) pro(h)phe(h)tí(h)am;(d:) O(h)lym(h)pí(h)a(h)de(h) cen(h)té(h)si(h)ma(h) no(h)na(h)gé(h)si(h)ma(h) quar(h)ta;(d:) ab(h) Ur(h)be(h) cón(h)di(h)ta(h,) an(h)no(h) sep(h)tin(h)gen(h)té(h)si(h)mo(h) quin(h)qua(h)gé(h)si(h)mo(h) se(h)cún(h)do;(d:) an(h)no(h) im(h)pé(h)ri(h)i(h) Cǽ(h)sa(h)ris(h) Oc(h)ta(h)vi(h)á(h)ni(h) Au(h)gús(h)ti(h,) qua(h)dra(h)gé(h)si(h)mo(h) se(h)cún(h)do;(d:) to(h)to(h) Or(h)be(h) in(h) pa(h)ce(h) com(h)pó(h)si(f)to,(f;) Ie(h)sus(h) Chris(h)tus, æ(h)tér(h)nus(h) De(h)us(h) æ(h)ter(h)ní(h)que(h) Pa(h)tris(h) Fí(h)li(h)us,(h,) mun(h)dum(h) vo(h)lens(h) ad(h)vén(h)tu(h) su(h)o(h) pi(h)ís(h)si(h)mo(h) con(h)se(h)crá(h)re,(h,) de(h) Spí(h)ri(h)tu(h) Sanc(h)to(h) con(h)cép(h)tus,(h) no(h)vém(h)que(h) post(h) con(h)cep(h)ti(h)ó(h)nem(h) de(h)cúr(h)sis(h) mén(h)si(h)bus,(h:) in(j) Béth(j)le(j)hem(j) Iu(j)dæ(j) nás(j)ci(j)tur(j) ex(j) Ma(j)rí(j)a(j) Vír(j)gi(j)ne(j) fac(j)tus(j) ho(j)mo:(gxg:) Na(k)tí(k)vi(k)tas(k) Dó(k)mi(k)ni(k) nos(k)tri(k) Ie(j)su(i) Chris(k)ti(k;) se(k)cún(k)dum(h) car(i)nem.(gxg::)'
let chantContainerElMar = document.getElementById('elogium_martyrologii')
let chantContainerElNat = document.getElementById('elogium_in_nativitate_domini')
let scoreElMar, scoreElNat

let ctxt = {
	black: new ex.black.ChantContext(),
	green: new ex.green.ChantContext()
}

// Configure fonts
ctxt.black.lyricTextFont = "'Libertinus Serif', 'Linux Libertine G', serif"
ctxt.black.alTextFont = ctxt.black.lyricTextFont
ctxt.black.annotationTextFont = ctxt.black.lyricTextFont
ctxt.black.dropCapTextFont = ctxt.black.lyricTextFont
ctxt.black.translationTextFont = ctxt.black.lyricTextFont
ctxt.green.lyricTextFont = "'Libertinus Serif', 'Linux Libertine G', serif"
ctxt.green.alTextFont = ctxt.green.lyricTextFont
ctxt.green.annotationTextFont = ctxt.green.lyricTextFont
ctxt.green.dropCapTextFont = ctxt.green.lyricTextFont
ctxt.green.translationTextFont = ctxt.green.lyricTextFont

// Configure colours
ctxt.black.lyricTextColor = '#000000'
ctxt.black.alTextColor = ctxt.black.lyricTextColor
ctxt.black.annotationTextColor = ctxt.black.lyricTextColor
ctxt.black.dividerLineColor = ctxt.black.lyricTextColor
ctxt.black.dropCapTextColor = ctxt.black.lyricTextColor
ctxt.black.neumeLineColor = ctxt.black.lyricTextColor
ctxt.black.staffLineColor = ctxt.black.lyricTextColor
ctxt.black.translationTextColor = ctxt.black.lyricTextColor
ctxt.black.rubricColor = '#ff0000'
ctxt.black.specialCharProperties.fill = '#ff0000'
ctxt.green.lyricTextColor = '#00ff00'
ctxt.green.alTextColor = ctxt.green.lyricTextColor
ctxt.green.annotationTextColor = ctxt.green.lyricTextColor
ctxt.green.dividerLineColor = ctxt.green.lyricTextColor
ctxt.green.dropCapTextColor = ctxt.green.lyricTextColor
ctxt.green.neumeLineColor = ctxt.green.lyricTextColor
ctxt.green.staffLineColor = ctxt.green.lyricTextColor
ctxt.green.translationTextColor = ctxt.green.lyricTextColor
ctxt.green.rubricColor = '#ff0000'
ctxt.green.specialCharProperties.fill = '#ff0000'

let updateChant = function(theme = 'green') {
	if (scoreElMar) {
		ex[theme].Gabc.updateMappingsFromSource(ctxt[theme], scoreElMar.mappings, gabcSourceElMar)
		scoreElMar.updateNotations(ctxt[theme])
	} else {
		mappings = ex[theme].Gabc.createMappingsFromSource(ctxt[theme], gabcSourceElMar)
		scoreElMar = new ex[theme].ChantScore(ctxt[theme], mappings, true)
	}

	if (scoreElNat) {
		ex[theme].Gabc.updateMappingsFromSource(ctxt[theme], scoreElNat.mappings, gabcSourceElNat)
		scoreElNat.updateNotations(ctxt[theme])
	} else {
		mappings = ex[theme].Gabc.createMappingsFromSource(ctxt[theme], gabcSourceElNat)
		scoreElNat = new ex[theme].ChantScore(ctxt[theme], mappings, true)
	}

	layoutChant(theme)
}

let layoutChant = function(theme = 'green') {
	// Perform layout on the chant
	scoreElMar.performLayoutAsync(ctxt[theme], function() {
		scoreElMar.layoutChantLines(ctxt[theme], chantContainerElMar.clientWidth, function() {
			// Render the score to SVG code
			chantContainerElMar.innerHTML = scoreElMar.createSvg(ctxt[theme])
		})
	})

	// Perform layout on the chant
	scoreElNat.performLayoutAsync(ctxt[theme], function() {
		scoreElNat.layoutChantLines(ctxt[theme], chantContainerElNat.clientWidth, function() {
			// Render the score to SVG code
			chantContainerElNat.innerHTML = scoreElNat.createSvg(ctxt[theme])
		})
	})
}

// Add or override `onload` and `onresize` event listeners
window.onload = function() {
	let theme = getParam('theme')

	try {
		setTheme(theme)
	} catch (err) {
		console.info('[INFO] Ignoring `setTheme()`, because it cannot be found.')
	}

	updateChant(theme)
}

window.addEventListener('resize', () => {
	layoutChant(theme)
})
