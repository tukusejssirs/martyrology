## Change Log

| **Date**          | **Changes** |
|-------------------|-------------|
| 30 August 2021    | Proofread and CSS style the text of image `124_mr2004.html` |
| 25 July 2021      | Proofread and CSS style the text of images `121_mr2004.html` through `123_mr2004.html` <br> Add individual dates to the sidebar (`partials/template.html`): <ul><li>note that although all dates are listed, only links to dates in January a functional </li></ul> |
| 13 July 2021      | Proofread and CSS style the text of image `120_mr2004.html` <br> Fix some typos in `077_mr2004.html`, `079_mr2004.html` and `081_mr2004.html` |
| 11 July 2021      | Proofread and CSS style the text of images `118_mr2004.html` through `119_mr2004.html` <br> Update `contributing.md` |
| 10 July 2021      | Proofread and CSS style the text of images `116_mr2004.html` through `117_mr2004.html` <br> Update `contributing.md` with quick guide how to proofread and style a page <br> Remove `696_mr2004.html` (it is an empty page) |
| 04 July 2021      | Proofread and CSS style the text of images `113_mr2004.html` through `115_mr2004.html` |
| 03 July 2021      | Proofread and CSS style the text of image `112_mr2004.html` |
| 01 July 2021      | Fix missing `dc` dependency in `install_dependencies.sh` |
| 01 July 2021      | Proofread and CSS style the text of images `110_mr2004.html` through `111_mr2004.html` <br> Update the release changelog with a list of done ranges (instead of just the last page done) |
| 28 June 2021      | Proofread and CSS style the text of images `107_mr2004.html` through `109_mr2004.html` |
| 27 June 2021      | Proofread and CSS style the text of image `106_mr2004.html` <br> Fix a typo in `091_mr2004.html` <br> Update `regex_replaces_typos.sh` (put each replacement pattern on a separate line and sort them alphabetically) |
| 24 June 2021      | Proofread and CSS style the text of images `103_mr2004.html` through `105_mr2004.html` |
| 29 May 2021       | Fix a styling issue in `102_mr2004.html` |
| 29 May 2021       | Proofread and CSS style the text of images `098_mr2004.html` through `102_mr2004.html` <br> Fix a small issue in `.gitlab-ci.yml` <br> Replace `&thinsp;` with `<span style="white-space: nowrap">&</span>` in `hocr/*.html` to make thin spaces non-breakable |
| 28 May 2021       | Proofread and CSS style the text of images `091_mr2004.html` through `097_mr2004.html` <br> Fix some typos in `hocr/078_mr2004.html`, `hocr/081_mr2004.html` and `hocr/089_mr2004.html` |
| 21 May 2021       | Proofread and CSS style the text of images `087_mr2004.html` through `090_mr2004.html` |
| 20 May 2021       | Proofread and CSS style the text of images `083_mr2004.html` through `086_mr2004.html` <br> Add `scripts/format_lunar_cycle.sh` and `scripts/regex_replaces_typos.sh` auxiliary scripts |
| 19 May 2021       | Proofread and CSS style the text of images `080_mr2004.html` through `082_mr2004.html` <br> Remove Junicode font dependency <br> Replace `oé` with precomposed `œ́` in `071_mr2004.html` and `js/gregorianChant.js` <br> Replace `témpore` with `tempóre` in all processed files <br> Hide header in `hocr/079_mr2004.html` <br> Update `errata.md` |
| 17 May 2021       | Proofread and CSS style the text of images `075_mr2004.html` through `079_mr2004.html` (`076_mr2004.html` is empty) |
| 16 May 2021       | Fix loading fonts in releases |
| 16 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 9 |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 8 |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 7 |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 6 |
| 15 May 2021       | Replace `p7zip` with `unzip` and `zip` |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 5 |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 4 |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 3 |
| 15 May 2021       | Debugging installation of depencencies in `.gitlab-ci.yml` 4 |
| 15 May 2021       | Debugging installation of depencencies in `.gitlab-ci.yml` 3 |
| 15 May 2021       | Debugging installation of depencencies in `.gitlab-ci.yml` 2 |
| 15 May 2021       | Debugging installation of depencencies in `.gitlab-ci.yml` |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` 2 |
| 15 May 2021       | Fix installing depencencies in `.gitlab-ci.yml` |
| 15 May 2021       | Generate HTML files with sidebar via `.gitlab-ci.yml` <br> Add `js/gregorianChant.js`, `js/hamburgerMenu.js`, `js/themeToggle.js` <br> Add `partials/template.html` <br> Add `scripts/clean.js`, `scripts/generateHtmlFiles.js`, `scripts/generateTxtFiles.sh`, `scripts/install_dependencies.sh`, `scripts/release.sh` <br> Move `hocr/mr2004.css` to `css/mr2004.css` <br> Add `hocr/000_mr2004.html` with image `img/000_front_cover.png` and `hocr/000_mr2004.html` with `img/999_back_cover.png` <br> Mark `071_mr2004.html` as done (it was already styled) <br> Use `oé` instead of non-UTF8 ligature in the Christmas eulogy chant when the chant is not output in `071_mr2004.html` <br> Delete files `072_mr2004.html` through `074_mr2004.html` as they are now part of `071_mr2004.html` <br> Set (and override when generated with sidebar) default `onload` and `resize` event handlers <br> Don’t count `hocr/999_mr2004.html` while getting the latest proofed and styled page number in the release changelog <br> Add `empty-lines` definition to `.yamllint.yml` <br> Remove accidently committed `go.tar.gz` file <br> Add `.eslintrc.js` <br> Update `readme.md` |
| 25 April 2021     | Revert installing `release-cli` 0.0.6 to latest version and try upgrading `go` to solve the `undefined: os.ReadFile` issue in `.gitlab-ci.yml` |
| 25 April 2021     | Try installing `release-cli` 0.0.6 again in `.gitlab-ci.yml` |
| 25 April 2021     | Revert installing `release-cli` 0.0.6 to latest version in `.gitlab-ci.yml` |
| 25 April 2021     | Quote `#` in string when it comes after a space in `.gitlab-ci.yml` |
| 25 April 2021     | Install `ed` in `.gitlab-ci.yml` |
| 25 April 2021     | Fix `npm` installation <br> Don't ouput `apt install` output in `.gitlab-ci.yml` |
| 25 April 2021     | Output `apt install` output in `.gitlab-ci.yml` |
| 25 April 2021     | Install `npm` in `.gitlab-ci.yml` |
| 25 April 2021     | Fix `go` command path in `.gitlab-ci.yml` |
| 25 April 2021     | Use `release-cli` 0.6.0 due to failing CI/CD jobs |
| 25 April 2021     | Mark `069_mr2004.html` as done (it was already styled) <br> Proofread and CSS style the text of images `071_mr2004.html` through `074_mr2004.html` (`070_mr2004.html` is empty) <br> Use `exsurge` to output Gregorian chants on `071_mr2004.html` through `074_mr2004.html` (note that these four files are merged into `071_mr2004.html` to simplify the Gregorian chant generation) |
| 13 March 2021     | Proofread and CSS style the text of images `067_mr2004.html` and `068_mr2004.html` <br> Update `errata.md` |
| 09 February 2021  | Proofread and CSS style the text of images `063_mr2004.html` through `066_mr2004.html` <br> Fix some CSS issues in `029_mr2004.html` and `031_mr2004.html` <br> Improve formatting of the progress part in release log in `.gitlab-ci.yml` <br> Update `mr2004.css`, `contributing.md` and `errata.md` |
| 08 February 2021  | Proofread and CSS style the text of images `058_mr2004.html` through `062_mr2004.html` (`062` is a blank page) <br> Fix a typo in a CSS class name in `048_mr2004.html` <br> Update `errata.md` |
| 07 February 2021  | Proofread and CSS style the text of images `053_mr2004.html` through `057_mr2004.html` <br> Fix a typo in `051_mr2004.html` <br> Update `errata.md` |
| 04 February 2021  | Proofread and CSS style the text of image `052_mr2004.html` |
| 03 February 2021  | Proofread and CSS style the text of images `050_mr2004.html` and `051_mr2004.html` <br> Fix `normal_foreground_colour` and `normal_background_colour` using `!important` (thus this works to override `heading_3` foreground colour) <br> Make scrollbar background transparent |
| 01 February 2021  | Proofread and CSS style the text of images `048_mr2004.html` and `049_mr2004.html` |
| 01 February 2021  | Update `mr2004.css`: <ul> <li> add wider left and right margins to the `<body>` </li> <li> change the table border widths to `4px` and `2px` for thick and thin border respectively in CSS </li> <li> simplify CSS `margin` definition </li> <li> use one `font-feature-settings` definition in `body` with multiple values </li> <li> fix zooming of the subtitle on the title page and all the tables in CSS </li> <li> Remove `-moz-font-feature-settings`, `-webkit-font-feature-settings` and `-moz-text-align-last` from `mr2004.css` </li> </ul> <br> Restructure and update `contributing.md` |
| 01 February 2021  | Fix building `MARTYROLOGY_BLACK_HTML` in `.gitlab_ci.yml` (remove a signle quote left unintentionally there) |
| 01 February 2021  | Fix building `MARTYROLOGY_BLACK_HTML` in `.gitlab_ci.yml` (remove some code left unintentionally there) <br> Fix building `MARTYROLOGY_TXT` file in `.gitlab_ci.yml` (`w3m` output ASCII text instead of UTF-8) |
| 01 February 2021  | Fix YAML syntax in `.gitlab_ci.yml` |
| 31 January 2021   | Add frontispiece image and image of the coat of arms of the Holy See and Vatican City to `img/` folder and add it to `hocr/002_mr2004.html` and `hocr/003_mr2004.html` respectively <ul><li> Note: The images are substituted with their `alt` text in the released HTML files </li></ul> <br> Fix `846_mr2004.html` (move `</body>` to a separate line and thus keep `</div>` in the HTML files attached to the releases) <br> Use POSIX `sh` shell syntax where necessary in `.gitlab_ci.yml` in order to make the commands more readable |
| 31 January 2021   | Fix format of the progress in release changelog in `.gitlab_ci.yml` 3 |
| 31 January 2021   | Fix format of the progress in release changelog in `.gitlab_ci.yml` 2 |
| 31 January 2021   | Fix format of the progress in release changelog in `.gitlab_ci.yml` |
| 31 January 2021   | Install `bc` in `.gitlab_ci.yml` |
| 31 January 2021   | Apply `hide_header` class to `div` only (not to `p`) in all pages <br> Add progress report to releases using `.gitlab_ci.yml` <br> Proofread and CSS style the text of image `047_mr2004.tif` |
| 25 January 2021   | Revert back to BASH one-liner in `.gitlab_ci.yml` |
| 25 January 2021   | Fix error in `.gitlab_ci.yml` 3 |
| 25 January 2021   | Fix error in `.gitlab_ci.yml` 2 |
| 25 January 2021   | Fix error in `.gitlab_ci.yml` |
| 25 January 2021   | Remove `txt/` folder <br> Fix some information in `contributing.md` and `readme.md` |
| 25 January 2021   | Use BASH in `.gitlab_ci.yml` |
| 25 January 2021   | Fix a typo in `.gitlab_ci.yml` <br> Add `.yamllint.yml` |
| 25 January 2021   | Create text file during the CI build |
| 25 January 2021   | Make installing `release-cli` quietly |
| 25 January 2021   | Try fixing uploading artifacts 33 |
| 25 January 2021   | Try fixing uploading artifacts 32 |
| 25 January 2021   | Try fixing uploading artifacts 31 |
| 25 January 2021   | Try fixing uploading artifacts 30 |
| 25 January 2021   | Try fixing uploading artifacts 29 |
| 25 January 2021   | Try fixing uploading artifacts 28 |
| 25 January 2021   | Try fixing uploading artifacts 27 |
| 25 January 2021   | Try fixing uploading artifacts 26 |
| 25 January 2021   | Try fixing uploading artifacts 25 |
| 25 January 2021   | Try fixing uploading artifacts 24 |
| 25 January 2021   | Try fixing uploading artifacts 23 |
| 25 January 2021   | Try fixing uploading artifacts 22 |
| 25 January 2021   | Try fixing uploading artifacts 20 |
| 25 January 2021   | Try fixing uploading artifacts 19 |
| 25 January 2021   | Try fixing uploading artifacts 18 |
| 25 January 2021   | Try fixing uploading artifacts 17 |
| 25 January 2021   | Try fixing uploading artifacts 16 |
| 25 January 2021   | Try fixing uploading artifacts 15 |
| 25 January 2021   | Try fixing uploading artifacts 14 |
| 25 January 2021   | Try fixing uploading artifacts 13 |
| 25 January 2021   | Try fixing uploading artifacts 12 |
| 25 January 2021   | Try fixing uploading artifacts 11 |
| 25 January 2021   | Try fixing uploading artifacts 10 |
| 25 January 2021   | Try fixing uploading artifacts 9 |
| 25 January 2021   | Try fixing uploading artifacts 8 |
| 25 January 2021   | Try fixing uploading artifacts 7 |
| 25 January 2021   | Try fixing uploading artifacts 6 |
| 25 January 2021   | Try fixing uploading artifacts 5 |
| 25 January 2021   | Try fixing uploading artifacts 4 |
| 25 January 2021   | Try fixing uploading artifacts 3 |
| 21 January 2021   | Try fixing uploading artifacts 2 |
| 21 January 2021   | Try fixing uploading artifacts |
| 21 January 2021   | Try fixing `release` script in `gitlab-ci.yml` 6 |
| 21 January 2021   | Try fixing `release` script in `gitlab-ci.yml` 5 |
| 21 January 2021   | Try fixing `release` script in `gitlab-ci.yml` 4 |
| 21 January 2021   | Try fixing `release` script in `gitlab-ci.yml` 3 |
| 20 January 2021   | Try fixing `release` script in `gitlab-ci.yml` 2 |
| 20 January 2021   | Try fixing `release` script in `gitlab-ci.yml` |
| 20 January 2021   | Try reverting `PACKAGE_VERSION` definition using variable in `gitlab-ci.yml` |
| 20 January 2021   | Try reverting `PACKAGE_VERSION` definition using variable and add `needs` of each script in `gitlab-ci.yml` |
| 20 January 2021   | Use variable in `PACKAGE_VERSION` value and fix the `upload` script `gitlab-ci.yml` |
| 20 January 2021   | Revert prettifying the `build` script commands in `gitlab-ci.yml` |
| 20 January 2021   | Try prettifying the `build` script commands and fix `upload script` in `gitlab-ci.yml` |
| 19 January 2021   | Try using `ubuntu` image with explicit BASH shell in `gitlab-ci.yml` |
| 19 January 2021   | Try using `alpine` image with explicit BASH shell in `gitlab-ci.yml` |
| 19 January 2021   | Try using `ubuntu` image in `gitlab-ci.yml` |
| 19 January 2021   | Fix validation issues in `gitlab-ci.yml` 2 |
| 19 January 2021   | Fix validation issues in `gitlab-ci.yml` |
| 19 January 2021   | Indent YAML files with two spaces |
| 19 January 2021   | Add CSS styled pages `013_mr2004.tif` through `027_mr2004.tif` <br> Use external `mr2004.css` stylesheet instead of internal one <br> Split `mr2004.html` into separate files by pages in order to use less RAM when editing that file (no files for blank pages were kept) <br> Use relative paths to images in `mr2004.html` <br> Change the green foreground colour to `#00ff00` instead of `#00aa00` <br> Add `.gitlab-ci.yml` <br> Fix an issue in `029_mr2004.html` (page number styled as footnote reference) <br> Update `readme.md`, `errata.md` and `contributing.md` |
| 16 January 2021   | Add OCR’ed text of images `028_mr2004.tif` through `846_mr2004.tif` <br> Proofread the text of images `028_mr2004.tif` through `046_mr2004.tif` <br> Fix `846_mr2004.tif` image in ScanTailor Advanced <br> Add CSS styled pages: <ul><li> `001_mr2004.tif` </li><li> `003_mr2004.tif` (without the Vatican coat of arms) </li><li> `004_mr2004.tif` through `012_mr2004.tif` </li><li> `029_mr2004.tif` through `046_mr2004.tif` </li></ul> <br> Add CSS styles for all first level headings <br> Modify indentation of `mr2004.html` to use tabs <br> Add `errata.md` <br> Update `readme.md` and `contributing.md` <br> Clean up `.editorconfig` <br> Add `test*` to `.gitignore` |
| 11 January 2021   | Add OCR’ed text of image `026_mr2004.tif` <br> Update `contributing.md` |
| 10 January 2021   | Use `tesseract` CLI to get hOCR data of `031_mr2004.tif` through `846_mr2004.tif` (manually updated the `<div title>` properties to match those generated by gImageReader) <br> Change `ppageno` and `id` (`page_x`) value to correspond with the real page number <br> Add OCR’ed text of images `023_mr2004.tif` through `025.mr2004.tif` <br> Update `contributing.md` |
| 08 January 2021   | Add OCR’ed text of images `016_mr2004.tif` through `022_mr2004.tif` |
| 06 January 2021   | Add OCR’ed text of images `014_mr2004.tif` through `015_mr2004.tif` |
| 05 January 2021   | Add OCR’ed text of the following images: <ul><li>`007_mr2004.tif`</li><li>`008_mr2004.tif`</li><li>`009_mr2004.tif`</li><li>`011_mr2004.tif`</li><li>`012_mr2004.tif`</li><li>`013_mr2004.tif`</li></ul> <br> Add non-proofread hOCR data of `014_mr2004.tif` through `030_mr2004.tif` <br> Update `.editorconfig` with gImageReader hOCR format (indentation and trailing newline rules) <br> Update `contributing.md` |
| 04 January 2021   | Add `readme.md`, `changelog.md`, `licence.md`, `processing_book.md`, `contributing.md` and `.editorconfig` |
| 03 January 2021   | Initial commit |